﻿using System;

namespace RationalNumbers
{
	/// <summary>
	/// Демонстрация работы программы
	/// </summary>
	class Program
	{
		static void Main(string[] args)
		{
			var a = new Rational(1, 2);
			var b = new Rational(1, 3);
			Print(a.ToString());
			Print(b.ToString());
			// Сложение, вычитание, умножение, деление дробей
			Print($"{a} + {b} = {(a + b)}");
			Print($"{a} - {b} = {(a - b)}");
			Print($"{b} - {a} = {(b - a)}");
			Print($"{a} * {b} = {(a * b)}");
			Print($"{a} / {b} = {(a / b)}");

			// IsNan
			var c = new Rational(3, 0);
			Print(c.ToString());
			Print(c.IsNan.ToString());

			// Сложение дроби с целым числом
			Print($"{a} + 5 = {(a + 5)}");
			Print($"5 + {a} = {(5 + a)}");

			// Умножение дроби на число
			Print($"5 * {a} = {(5 * a)}");
			Print($"{a} * 5 = {(a * 5)}");

			// Вычитание дроби и числа
			Print($"{a} - 2 = {(a - 2)}");
			Print($"2 - {a} = {(2 - a)}");

			// Инкремент, декремент
			Print($"{a}++ = {a++}");
			Print($"{a}++ = {a++}");
			Print($"{a}++ = {a++}");

			var d = new Rational(3, 2);
			if (d)
			{
				Print($"{d} is True");
			}
			else
			{
				Print($"{d} is False");
			}

			// Логические операторы
			var f = new Rational(1, 3);
			Print($"{b} == {f} => {b == f}");
			Print($"{a} == {f} => {a == f}");
			Print($"{a} > {f} => {a > f}");

			// Приведение
			Print($"{(int)a}");
			
			// Индексатор
			Print($"a[0] == {a[0]}");
			Print($"a[1] == {a[1]}");

			Console.ReadKey();
		}

		static void Print(string input)
		{
			Console.WriteLine(input);
		}
	}
}
