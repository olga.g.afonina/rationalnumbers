﻿using System;

namespace RationalNumbers
{
	/// <summary>
	/// Класс работы с рациональными числами
	/// Рациональное число  — число, которое можно представить обыкновенной дробью, числитель m — целое число, а знаменатель n — натуральное число.
	/// </summary>
	public class Rational
	{
		#region Properties
		/// <summary>
		/// Числитель, целое число
		/// </summary>
		public int Numerator { get; private set; }

		/// <summary>
		/// Знаменатель, натуральное число
		/// </summary>
		public int Denominator { get; private set; }

		/// <summary>
		/// Пример реализации индексатора
		/// В данном классе абсолютно бессмыссленный, но да ладно...
		/// 0 - числитель
		/// 1 - знаменатель
		/// </summary>
		public int this[int index]
		{
			get
			{
				if (index == 0)
				{
					return Numerator;
				}

				if (index == 1)
				{
					return Denominator;
				}

				throw new ArgumentOutOfRangeException($"{nameof(index)} должен быть 0 или 1");
			}
		}
		public bool IsNan => Denominator == 0;

		#endregion

		#region Constructors
		/// <summary>
		/// Конструктор с параметрами
		/// </summary>
		/// <remarks>
		/// По возможности сокращаем дробь
		/// </remarks>
		/// <param name="numerator"></param>
		/// <param name="denominator"></param>
		public Rational(int numerator, int denominator = 1)
		{
			if (denominator == 0)
			{
				numerator = 1;
			}
			else if (numerator == 0)
			{
				denominator = 1;
			}
			else if (denominator < 0)
			{
				// Числитель и знаменатель отрицательные => дробь будет положительная
				if (numerator < 0)
				{
					numerator = Math.Abs(numerator);
					denominator = Math.Abs(denominator);
				}
				// Знаменатель отрицательный, числитель положительный => поменять местами знак, т.к. по условию знаменатель - положительное число
				else
				{
					numerator = -1 * numerator;
					denominator = -1 * denominator;
				}
			}

			// Сокращаем дробь, присваиваем значения свойствам
			Simplify(numerator, denominator);
		}
		#endregion

		#region Methods
		/// <summary>
		/// Сокращение дроби
		/// </summary>
		private void Simplify(int numerator, int denominator)
		{
			int gcd = GCD(numerator, denominator);
			if (gcd != 0)
			{
				Numerator = numerator / gcd;
				Denominator = denominator / gcd;
			}
		}

		/// <summary>
		/// Наибольший общий делитель - максимальное число, на которое делятся числитель и знаменатель
		/// </summary>
		public int GCD(int numerator, int denominator)
		{
			int n = Math.Abs(numerator);
			int d = Math.Abs(denominator);

			while (n != 0 && d != 0)
			{
				if (n > d)
				{
					n = n % d;
				}
				else
				{
					d = d % n;
				}
			}

			return n + d;
		}

		/// <summary>
		/// Наименьший общий знаменатель
		/// </summary>
		private static int LCD(int a, int b)
		{
			// Если равны
			if (a == b)
			{
				return a;
			}

			int max = Math.Max(a, b);
			int min = Math.Min(a, b);

			// Если максимальное число делится без остатка на минимальное
			if (max / min == 0)
			{
				return max;
			}

			int i = 2;
			while (max * i / min == 0)
			{
				i++;
			}

			return max * i;
		}

		public override string ToString()
		{
			return $"{Numerator}/{Denominator}";
		}

		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			Rational r = obj as Rational;
			if (r == null)
			{
				return false;
			}

			return r.Numerator == Numerator && r.Denominator == Denominator;
		}

		/// <summary>
		/// Также для повышения производительности при переопределении метода Equals рекомендуется перегружать его реализацией
		/// с типом аргумента соответствующему классу, в котором он переопределяется
		/// </summary>
		public bool Equals(Rational obj)
		{
			if (obj == null)
			{
				return false;
			}

			return obj.Numerator == Numerator && obj.Denominator == Denominator;
		}

		// Как правильно переопределить GetHashCode?
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		#endregion

		#region Operators

		public static Rational operator *(Rational rational, int num)
			=> new Rational(rational.Numerator * num, rational.Denominator);

		public static Rational operator *(int num, Rational rational)
			=> rational * num;

		public static Rational operator +(Rational rational, int num)
			=> new Rational(rational.Denominator * num + rational.Numerator, rational.Denominator);

		public static Rational operator +(int num, Rational rational)
			=> rational + num;

		public static Rational operator -(Rational rational, int num)
			=> rational + -1 * num;

		public static Rational operator -(int num, Rational rational)
			=> num + -1 * rational;

		// Просто добавим единицу
		public static Rational operator ++(Rational rational)
			=> rational + 1;

		// Просто отнимем единицу
		public static Rational operator --(Rational rational)
			=> rational - 1;

		public static Rational operator +(Rational a, Rational b)
		{
			if (a.Denominator == 0 || b.Denominator == 0)
			{
				return new Rational(1, 0);
			}

			var lcd = LCD(a.Denominator, b.Denominator);
			return new Rational(lcd / a.Denominator * a.Numerator + lcd / b.Denominator * b.Numerator, lcd);
		}

		public static Rational operator -(Rational a, Rational b)
			=> a + -1 * b;

		public static Rational operator *(Rational a, Rational b)
		{
			if (a.Denominator == 0 || b.Denominator == 0)
			{
				return new Rational(1, 0);
			}

			return new Rational(a.Numerator * b.Numerator, a.Denominator * b.Denominator);
		}

		public static Rational operator /(Rational a, Rational b)
		{
			if (a.Denominator == 0 || b.Denominator == 0 || b.Numerator == 0)
			{
				return new Rational(1, 0);
			}

			return a * new Rational(b.Denominator, b.Numerator);
		}

		public static bool operator ==(Rational a, Rational b)
			=> a.Numerator == b.Numerator && a.Denominator == b.Denominator;

		public static bool operator !=(Rational a, Rational b)
			=> a.Numerator != b.Numerator || a.Denominator != b.Denominator;

		public static bool operator >(Rational a, Rational b)
			=> (a - b).Numerator > 0;

		public static bool operator <(Rational a, Rational b)
			=> (a - b).Numerator < 0;

		public static bool operator <=(Rational a, Rational b)
			=> (a - b).Numerator <= 0;

		public static bool operator >=(Rational a, Rational b)
			=> (a - b).Numerator >= 0;

		public static bool operator false(Rational rational)
			=> rational.Numerator > rational.Denominator;

		public static bool operator true(Rational rational)
			=> rational.Numerator < rational.Denominator;
		#endregion

		#region Explicit / implicit
		public static explicit operator int(Rational a)
		{
			if (a.Denominator == 0)
				throw new ArgumentOutOfRangeException($"{nameof(Denominator)} не может быть 0");
			return (int)(a.Numerator / a.Denominator);
		}

		public static implicit operator double(Rational a)
			=> (double)a.Numerator / a.Denominator;

		public static implicit operator Rational(int a)
			=> new Rational(a);
		#endregion
	}
}
